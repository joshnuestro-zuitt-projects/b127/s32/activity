const express = require('express')
const mongoose = require('mongoose')
//Allows our backend application to be available to our frontend application
//Allows us to control app's cross-origin resource sharing settings
const cors = require('cors')
const app = express()

mongoose.connect('mongodb+srv://admin:admin@zuitt-bootcamp.vlaci.mongodb.net/Batch127_Booking?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', ()=>console.log('Now connected to MongoDB Atlas'))

//Allows all resources/origins to access our backend application
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))

// app.listen(process.env.PORT || 4000, ()=>{
// 	console.log(`API is now online on port ${process.env.PORT || 4000}`)
// })

// app.listen(process.env.PORT || 4000, ()=>{
// 	console.log(`
// ⠸⣷⣦⠤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⣠⣤⠀⠀⠀
// ⠀⠙⣿⡄⠈⠑⢄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⠔⠊⠉⣿⡿⠁⠀⠀⠀
// ⠀⠀⠈⠣⡀⠀⠀⠑⢄⠀⠀⠀⠀⠀⠀⠀⠀⠀⡠⠊⠁⠀⠀⣰⠟⠀⠀⠀⣀⣀
// ⠀⠀⠀⠀⠈⠢⣄⠀⡈⠒⠊⠉⠁⠀⠈⠉⠑⠚⠀⠀⣀⠔⢊⣠⠤⠒⠊⠉⠀⡜
// ⠀⠀⠀⠀⠀⠀⠀⡽⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠩⡔⠊⠁⠀⠀⠀⠀⠀⠀⠇
// ⠀⠀⠀⠀⠀⠀⠀⡇⢠⡤⢄⠀⠀⠀⠀⠀⡠⢤⣄⠀⡇⠀⠀⠀⠀⠀⠀⠀⢰⠀
// ⠀⠀⠀⠀⠀⠀⢀⠇⠹⠿⠟⠀⠀⠤⠀⠀⠻⠿⠟⠀⣇⠀⠀⡀⠠⠄⠒⠊⠁⠀          "PIKA PIKA" 
// ⠀⠀⠀⠀⠀⠀⢸⣿⣿⡆⠀⠰⠤⠖⠦⠴⠀⢀⣶⣿⣿⠀⠙⢄⠀⠀⠀⠀⠀⠀
// ⠀⠀⠀⠀⠀⠀⠀⢻⣿⠃⠀⠀⠀⠀⠀⠀⠀⠈⠿⡿⠛⢄⠀⠀⠱⣄⠀⠀⠀⠀
// ⠀⠀⠀⠀⠀⠀⠀⢸⠈⠓⠦⠀⣀⣀⣀⠀⡠⠴⠊⠹⡞⣁⠤⠒⠉⠀⠀⠀⠀⠀
// ⠀⠀⠀⠀⠀⠀⣠⠃⠀⠀⠀⠀⡌⠉⠉⡤⠀⠀⠀⠀⢻⠿⠆⠀⠀⠀⠀⠀⠀⠀(Translation: Port ${process.env.PORT || 4000})
// ⠀⠀⠀⠀⠀⠰⠁⡀⠀⠀⠀⠀⢸⠀⢰⠃⠀⠀⠀⢠⠀⢣⠀⠀⠀⠀⠀⠀⠀⠀
// ⠀⠀⠀⢶⣗⠧⡀⢳⠀⠀⠀⠀⢸⣀⣸⠀⠀⠀⢀⡜⠀⣸⢤⣶⠀⠀⠀⠀⠀⠀
// ⠀⠀⠀⠈⠻⣿⣦⣈⣧⡀⠀⠀⢸⣿⣿⠀⠀⢀⣼⡀⣨⣿⡿⠁⠀⠀⠀⠀⠀⠀
// ⠀⠀⠀⠀⠀⠈⠻⠿⠿⠓⠄⠤⠘⠉⠙⠤⢀⠾⠿⣿⠟⠋ 
// 		`)
// })

app.listen(process.env.PORT || 4000, ()=>{
	console.log(`
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠟⠛⠛⠛⢛⠻⠿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠉⠐⠄⠄⠄⠄⠠⠄⠄⢀⣀⡈⢻⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠋⡀⠴⢒⡒⠂⠄⢀⡀⢐⡀⡀⠄⠄⢀⠙⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⡿⡡⠪⠶⢿⡭⡉⠄⢀⢣⣖⣶⣬⡱⣄⠄⠄⠄⠸⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⡁⣔⣎⠓⠂⡀⠑⠖⣒⠭⠻⠿⠿⠷⠙⣎⠉⠉⠄⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⢧⢱⣿⣔⣀⠊⠐⢼⣿⣶⣠⠄⠐⡐⠶⡂⣿⣀⡀⠄⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣟⠂⣿⣿⣷⣶⣟⣴⣾⣿⣧⣐⣤⣤⣭⣿⠇⡇⠄⠄⠄⣿⣿⣿
⣿⣿⣿⣿⣿⣿⢹⠄⣿⣿⣿⣿⡌⠿⣛⣿⣿⣿⣿⣿⣿⢫⡞⡇⠄⠄⢰⣿⣿⣿
⣿⣿⣿⣿⡿⣫⠞⢀⣿⣿⣿⣥⣤⣌⠙⣿⣿⣿⣿⡟⣽⣿⠁⡇⠒⢒⣾⣿⣿⣿ What the hell is this?
⣿⣿⣿⣯⣾⠋⠄⠘⣿⣿⣿⣶⣶⣭⣿⣿⣿⣿⣿⣾⡿⠃⣴⡇⠄⣼⣿⣿⣿⣿ 
⡛⡿⣿⣿⣷⣇⠄⠄⡈⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣉⠄⣼⡇⣼⣿⣿⣿⣿⣿ 
⠉⠐⣥⡙⠛⠿⠇⢸⠰⠄⠄⠄⠹⠟⠛⠋⢉⣀⢩⢴⠄⢰⣿⣧⢿⣿⣿⣿⣿⣿
⠄⠄⠄⢱⡀⠄⠄⠇⡰⠄⡐⠄⠰⠰⢿⠃⠈⠉⢠⠄⠄⠻⠿⢾⣿⣿⣿⣿⣿⣿
⡤⡴⠶⠚⠋⠻⣆⠄⠁⣠⠡⠐⠁⠄⠄⠄⠄⠄⠁⠄⠠⣦⠄⠄⠄⠄⠄⠲⠎⠉
		`)
})